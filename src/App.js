import React, { Component } from 'react';

import './App.css';
import Minion from './components/Minion';
// import logo from './logo.png'

import Aux from './hoc/Auxil';
import Landing from './components/Landing';
// import Header from './templates/Header';

class App extends Component {
  // constructor(props) {
  //   super(props);
  //   this.handleChange = this.handleChange.bind(this)
  //   this.handleClick = this.handleClick.bind(this)
  // }
  state = {
    name: 'There!',
    greeting: 'Can I meet you?',
    chatButton: false,
    fourLetters: null,
    loading: false,
    landing: true,
    showModal:false,

  }
  handleDrop = () => {
    this.setState({
  showModal:true
})
  }


  handleChange = (e) => {
    const regex = /[a-zA-Z]/;
    const bool = regex.test(e.target.value)

    // eslint-disable-next-line
    if (e.target.value == '') {
      this.setState({
        name: 'There!',
        greeting: 'Can I meet you?',
        fourLetters: null,
        chatButton: false,
      });

    } else if (e.target.value.length <= 3) {
      this.setState({
        name: 'There!',
        greeting: 'Can I meet you?',
        fourLetters: 'Your name should be at least four letters long',
        chatButton: false,

      });
    } else {
      if (!bool) {
        this.setState({
          name: 'There!',
          greeting: 'Can I meet you?'

        });
      } else {
        this.setState({
          name: e.target.value + '!',
          greeting: 'Nice to meet you.',
          fourLetters: null,
          chatButton: true,

        });
      }
      // console.log(e.target.value);
    }
  }

  handleClick = () => {
    this.setState({
      loading: true,
      // landing: setTimeout((this.setState({
        landing:false
      // }), 5000))

    })
  }
  
  render() {
    return (
      <Aux>
       { this.state.landing ? 
        <Landing
          clicked={this.handleClick.bind(this)}
          changed={this.handleChange.bind(this)}
          name={this.state.name}
          greeting={this.state.greeting}
          loading={this.state.loading}
          chatButton={this.state.chatButton}
          fourLetters={this.state.fourLetters}
        /> :
      
        <Minion name={this.state.name} />}
      </Aux>
    );
  }
}

export default App;
