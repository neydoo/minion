import React, { Component } from 'react';

import '../assets/css/normalize.css';
import '../assets/css/style.css';

class SendMessageForm extends Component {
    state = {
        send:false,
    }

    handleChange = (e) => {
        if (e.target.value.length >= 1) {
            this.setState({
                send: true,
            });
        }
    }

    render() {
        return (
            <div>
                <div className='col-md-12 input-group input-group-sm'>
                <input
                    className="form-control form-control-sm"
                    type='text' placeholder='Type YourMessage'
                    value={this.value}
                    onChange={this.handleChange}
                />
                <div className="input-group-append">
                    {/* {this.props.loading ? <button className=" btn btn-success">
                        <i className="fa fa-spinner fa-spin"></i>Loading
                                            </button> : */}
                        <button
                            className="btn btn-primary"
                            onClick={this.handleClick}
                            type="submit"
                            disabled={!this.state.send}
                        >Send</button>
                </div>
                </div>
                
            </div>
        );
    }
}

export default SendMessageForm;