import React, { Component } from 'react';

// import Minion from '../components/Minion';
import Minions from '../assets/images/minions.gif';
// import Loader from '../UI/Loader';

class Landing extends Component {
    // constructor(props) {
    //     super(props);
    // }
    
    render() {
        return (
            <div id='top' >
                {/* <Minion /> */}
                <section id="intro">

                    <div className="intro-overlay"></div>

                    <div className="intro-content">
                        <div className="row">

                            <div className="col-twelve">

                                <h5>Hello, {this.props.name}</h5>
                                <h1>I'm Minion</h1><span> <img alt='' src={Minions} /></span>

                                <p className="intro-position">
                                    <span>{this.props.greeting}</span>
                                </p>
                                <p style={{color:'red'}}>{this.props.fourLetters}</p>
                                <div className='col-md-12 input-group input-group-sm'>
                                    <div className='col-md-3'></div>
                                    <input
                                        className="form-control form-control-sm"
                                        type='text' placeholder='Your name goes here'
                                        value={this.props.value}
                                    onChange={this.props.changed} />
                                    <div className="input-group-append">
                                        {this.props.loading ? <button className=" btn btn-success">
                                            <i className="fa fa-spinner fa-spin"></i>Loading
                                            </button> :
                                            <button
                                            className="btn btn-success"
                                            onClick={this.props.clicked}
                                            type="submit"
                                            disabled={!this.props.chatButton}
                                        >Go</button>}
                                    </div>
                                    <div className='col-md-3'></div>
                                </div>
                            </div>

                        </div>
                    </div>
                   
                </section>
            </div>
        )
    }
}


export default Landing;

