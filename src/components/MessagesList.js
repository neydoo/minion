import React, { Component } from 'react';

import '../assets/css/normalize.css';
import '../assets/css/style.css';

class MessagesList extends Component {
    render() {
        return (
            <ul className="messge-list">
                {this.props.messages.map(message => {
                    return (
                        <li key={message.id} className='message' >
                            <div>
                                {message.senderId}
                            </div>
                            <div>
                                {message.text}
                            </div>
                        </li>
                    )
            })}
            </ul>
        );
}
}  

export default MessagesList;