import React, { Component } from 'react';


import Header from '../templates/Header';
import MessagesList from './MessagesList';
import SendMessageForm from './SendMessageForm';

import Chatkit from '@pusher/chatkit';

const chatToken = new Chatkit.TokenProvider({
    url: 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/7765bc8b-ea95-4dc5-9885-6aff4bb7ba1d/token'
})
const roomId = '18446609';



class Minion extends Component {
    
    state = {
        messages:[],
        
    }
    
    componentDidMount() {
        
        const chatManager = new Chatkit.ChatManager({
            instanceLocator: "v1:us1:7765bc8b-ea95-4dc5-9885-6aff4bb7ba1d",
            userId: "Minion",
            tokenProvider: chatToken,
        })

        chatManager.connect()
            .then(currentUser => {
                this.currentUser = currentUser
                this.currentUser.subscribeToRoom({
                    roomId: currentUser.rooms[0].id,
                    hooks: {
                        onNewMessage: message => {
                            this.setState({
                               messages:[...this.state.messages, message]
                           })
                        }
                    }
                });
            })
            .catch(error => {
                console.error("error:", error);
            });
    }

    sendMessage(text) {
        this.currentUser.sendMessage({
            text,
            roomId: roomId
        })
    }
    
    render() {
        return (
            <div className='app' >
                <Header name={this.props.name} />
                <MessagesList
                    messages={this.state.messages}
                    roomId={this.state.roomId}
                    
                />
                <SendMessageForm  />
            </div>
        );
}
}  

export default Minion;
