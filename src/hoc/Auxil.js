import React from 'react';

const auxil = (props) => {
    return (
        <div>
            {props.children}
        </div>
    );
}

export default auxil;